import 'package:android_eropa/base.dart';
import 'package:flutter/material.dart';

class Sales extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: Center(
        child: Text(
          "Sales",
          style: TextStyle(
              fontSize: 50.0, color: Palette.red_eropa.withOpacity(0.2)),
        ),
      ),
    );
  }
}
