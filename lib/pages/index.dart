import 'package:android_eropa/base.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';

class Index extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          width: 0.6 * MediaQuery.of(context).size.width,
          height: 0.15 * MediaQuery.of(context).size.height,
          child: BarcodeWidget(
            barcode: Barcode.code128(), // Barcode type and settings
            data: '081234567890',
          ),
        ),
        Container(
          width: 0.75 * MediaQuery.of(context).size.width,
          height: 0.1 * MediaQuery.of(context).size.height,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 10.0,
            child: Center(
              child: Text(
                "Cek Kupon",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Palette.red_eropa.withOpacity(0.8)),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
