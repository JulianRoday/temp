import 'package:android_eropa/base.dart';
import 'package:android_eropa/pages/coupon.dart';
import 'package:android_eropa/pages/index.dart';
import 'package:android_eropa/pages/profile.dart';
import 'package:android_eropa/pages/sales.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  static String tag = 'home';
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  @override
  void initState() {
    _updateAppbar();
    super.initState();
  }

  void _updateAppbar() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  int _currentIndex = 0;
  final List<Widget> _children = [
    Index(),
    Coupon(),
    Sales(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          physics: NeverScrollableScrollPhysics(),
          slivers: [
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: CustomSliverDelegate(
                expandedHeight: 200,
                selectedIndex: _currentIndex,
              ),
            ),
            SliverFillRemaining(
              child: _children[_currentIndex],
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Palette.red_eropa,
          border: Border(
            top: BorderSide(color: Palette.red_eropa, width: 1.0),
            left: BorderSide(color: Palette.red_eropa, width: 1.5),
            right: BorderSide(color: Palette.red_eropa, width: 1.5),
          ),
        ),
        child: BottomNavigationBar(
          iconSize: 70 * MediaQuery.of(context).size.aspectRatio,
          currentIndex: _currentIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          unselectedItemColor: Palette.red_eropa.withOpacity(0.5),
          selectedItemColor: Palette.red_eropa,
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.redeem),
              title: Text('Coupon'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.open_in_browser),
              title: Text('History'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.perm_identity),
              title: Text('Profile'),
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
      ),
    );
  }
}

class CustomSliverDelegate extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final int selectedIndex;
  final bool hideTitleWhenExpanded;

  CustomSliverDelegate({
    @required this.expandedHeight,
    this.hideTitleWhenExpanded = true,
    this.selectedIndex,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final appBarSize = expandedHeight - shrinkOffset;
    final cardTopPosition = expandedHeight / 2 - shrinkOffset;
    final proportion = 2 - (expandedHeight / appBarSize);
    final percent = proportion < 0 || proportion > 1 ? 0.0 : proportion;
    return SizedBox(
      height: expandedHeight + expandedHeight / 2,
      child: Stack(
        children: [
          SizedBox(
            height: appBarSize < kToolbarHeight ? kToolbarHeight : appBarSize,
            child: AppBar(
              backgroundColor: Palette.red_eropa.withOpacity(0.8),
              automaticallyImplyLeading: selectedIndex == 0 ? false : true,
              elevation: 0.0,
              title: Center(
                child: Opacity(
                  opacity: selectedIndex == 0 ? 1.0 : 0.0,
                  child: Column(children: [
                    Text("Welcome!"),
                    Text("JANE DOE"),
                  ]),
                ),
              ),
            ),
          ),
          Positioned(
            left: 0.0,
            right: 0.0,
            top: cardTopPosition > 0 ? cardTopPosition : 0,
            bottom: 0.0,
            child: Opacity(
              opacity: selectedIndex == 0 ? percent : 0,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 70 * percent),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 10.0,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            'Rewards',
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Text(
                          "77",
                          style: TextStyle(
                            fontSize: 60,
                            fontWeight: FontWeight.bold,
                            color: Palette.red_eropa.withOpacity(0.8),
                          ),
                        ),
                        Text(
                          "Poin",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight + expandedHeight / 2;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
